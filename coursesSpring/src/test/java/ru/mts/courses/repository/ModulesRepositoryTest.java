package ru.mts.courses.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.domain.Module;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ModulesRepositoryTest {

    @Autowired
    private ModulesRepository modulesRepository;

    @Test
    @DisplayName("Проверка репозитория ModulesRepository")
    void ModulesRepositoryTest() {
        Module module = new Module(1L,"Test module", "module", null, null );
        modulesRepository.save(module);
        assertEquals(modulesRepository.count(),1);
    }

}
