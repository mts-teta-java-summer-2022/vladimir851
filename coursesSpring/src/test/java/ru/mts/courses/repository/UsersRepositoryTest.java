package ru.mts.courses.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.domain.Role;
import ru.mts.courses.domain.User;


import java.util.Date;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UsersRepositoryTest {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RolesRepository rolesRepository;

    @BeforeEach
    void cleanDatabase() {
        rolesRepository.deleteAll();
        usersRepository.deleteAll();
    }

    @Test
    @DisplayName("Проверка репозиториев userRepository и RolesRepository")
    void userRepositiryTest() {
        User user = new User(1L, "user", "Ivanov","Ivan", null, new Date(1212121212121L), "123",null,null );
        Role role = new Role(1L, "ROLE_ADMIN", Set.of(user));
        usersRepository.save(user);
        rolesRepository.save(role);
        assertEquals(usersRepository.count(),1);
        assertEquals(rolesRepository.count(),1);
    }
}
