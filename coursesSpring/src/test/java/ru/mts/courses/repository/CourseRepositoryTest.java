package ru.mts.courses.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import ru.mts.courses.domain.Course;
import ru.mts.courses.domain.CourseCategory;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CourseRepositoryTest {
    @Autowired
    private CoursesRepository courseRepository;

    @Autowired
    private CourseCategoriesRepository courseCategoriesRepository;

    @BeforeEach
    void cleanDatabase() {
        courseRepository.deleteAll();
        courseCategoriesRepository.deleteAll();
    }

    @Test
    @DisplayName("Проверка репозиториев courseRepository , courseCategoriesRepository")
    void CourseRepositoryTest() {
        CourseCategory courseCategory = new CourseCategory(1L,"Test category", null);
        Course course = new Course(1L, "author", "title", 3L , null, true, null,null,courseCategory);
        courseCategoriesRepository.save(courseCategory);
        courseRepository.save(course);
        assertEquals(courseCategoriesRepository.count(),1);
        assertEquals(courseRepository.count(),1);
    }
}