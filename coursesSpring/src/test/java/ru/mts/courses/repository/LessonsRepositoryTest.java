package ru.mts.courses.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.domain.Lesson;


import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LessonsRepositoryTest {

    @Autowired
    private LessonsRepository lessonsRepository;

    @Test
    @DisplayName("Проверка репозитория LessonsRepository")
    void LessonsRepositoryTest() {
        Lesson lesson = new Lesson(1L,"Test lesson", "lesson", null );
        lessonsRepository.save(lesson);
        assertEquals(lessonsRepository.count(),1);
    }
}
