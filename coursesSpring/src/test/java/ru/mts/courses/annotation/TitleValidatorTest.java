package ru.mts.courses.annotation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TitleValidatorTest {

    TitleCaseValidator validator = new TitleCaseValidator();

    // RU LANG Tests

    @Test
    @DisplayName("RU Позитивный тест. Первое слово с большой остальные с маленькой")
    void ru_positive_test() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertTrue(validator.isValid("Заголовок обычный", null));
    }

    @Test
    @DisplayName("RU Заголовок с запрещенными символами")
    void ru_forbidden_symbols_test() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertFalse(validator.isValid("Загол\\tовок\\n", null));
    }

    @Test
    @DisplayName("RU Пробел в начале строки")
    void ru_spaces_test_1() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertFalse(validator.isValid(" Тест", null));
    }

    @Test
    @DisplayName("RU Пробел в конце строки")
    void ru_spaces_test_2() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertFalse(validator.isValid("Тест ", null));
    }

    @Test
    @DisplayName("RU Более одного пробела в середине строки")
    void ru_spaces_test_3() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertFalse(validator.isValid("Тест  заголовок", null));
    }

    @Test
    @DisplayName("RU Смешение языков")
    void ru_mix_lang_test() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertFalse(validator.isValid("Тест TITLE", null));
    }

    @Test
    @DisplayName("RU тесты на знаки пунктуации")
    void ru_punctuation() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertTrue(validator.isValid("Заголовок, \"правильный\", валидацию: пройдет", null));
    }

    @Test
    @DisplayName("RU небуквенные символы")
    void ru_not_char() {
        validator.setLang(TitleCaseValidator.Lang.RU);
        assertFalse(validator.isValid("Заголовок-такой", null));
    }

    // EN LANG Tests

    @Test
    @DisplayName("EN Позитивный тест. Все слова с заглавной буквы кроме слов-исключений")
    void en_positive_test() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertTrue(validator.isValid("Title or not Title", null));
    }


    @Test
    @DisplayName("EN Заголовок с запрещенными символами")
    void en_forbidden_symbols_test() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertFalse(validator.isValid("Title\\tok\\n", null));
    }

    @Test
    @DisplayName("EN Пробел в начале строки")
    void en_spaces_test_1() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertFalse(validator.isValid(" Title", null));
    }

    @Test
    @DisplayName("EN Пробел в конце строки")
    void en_spaces_test_2() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertFalse(validator.isValid("Title ", null));
    }


    @Test
    @DisplayName("EN Более одного пробела в середине строки")
    void en_spaces_test_3() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertFalse(validator.isValid("Title  New", null));
    }


    @Test
    @DisplayName("EN Смешение языков")
    void en_mix_lang_test() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertFalse(validator.isValid("Тест TITLE", null));
    }

    @Test
    @DisplayName("EN тесты на знаки пунктуации")
    void en_punctuation() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertTrue(validator.isValid("Title, \"Correct\", Validation: Ok", null));
    }

    @Test
    @DisplayName("ANY На русском")
    void any_postitive1() {
        validator.setLang(TitleCaseValidator.Lang.EN);
        assertFalse(validator.isValid("Title-incorrect", null));
    }

    // ANY LANG Tests

    @Test
    @DisplayName("ANY на английском")
    void any_positive2() {
        validator.setLang(TitleCaseValidator.Lang.ANY);
        assertTrue(validator.isValid("Зголовок обычный", null));
    }

    @Test
    @DisplayName("ANY небуквенные символы")
    void en_not_char() {
        validator.setLang(TitleCaseValidator.Lang.ANY);
        assertTrue(validator.isValid("Title Correct", null));
    }


}
