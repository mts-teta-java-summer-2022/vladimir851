package ru.mts.courses;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public class IntegrationSuite {
    private static final DockerImageName POSTGRES_IMAGE = DockerImageName.parse("14.5");

    @Container
    public final PostgreSQLContainer<?> POSTGREES = new PostgreSQLContainer<>(POSTGRES_IMAGE);

}
