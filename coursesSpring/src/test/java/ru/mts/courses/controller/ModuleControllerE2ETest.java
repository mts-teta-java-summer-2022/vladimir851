package ru.mts.courses.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.mts.courses.repository.ModulesRepository;
import ru.mts.courses.domain.Module;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

@ExtendWith(SpringExtension.class)
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
public class ModuleControllerE2ETest {
    HttpHeaders headers = new HttpHeaders();
    TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int port;

    @Autowired
    private ModulesRepository modulesRepository;

    @Test
    @DisplayName("WebMvcTest для ModuleController метод get. Позитивный тест")
    @WithMockUser(roles = "ADMIN")
    public void getModuleTest() {
        Module module = new Module(1L, "123", "ddd",null, null);
        modulesRepository.save(module);
        get(createURLWithPort("/api/v1/modules/1")).then().statusCode(200).assertThat()
                .body("title", equalTo("123"));

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
}
