package ru.mts.courses.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.mts.courses.domain.Lesson;
import ru.mts.courses.repository.LessonsRepository;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;



@ExtendWith(SpringExtension.class)
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
public class LessonControllerE2ETest {

    HttpHeaders headers = new HttpHeaders();
    TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int port;

    @Autowired
    private LessonsRepository lessonsRepository;

    @Test
    @DisplayName("WebMvcTest для LessonController метод get. Позитивный тест")
    @WithMockUser(roles = "ADMIN")
    public void getLessonTest() {
        Lesson lesson = new Lesson(1L, "123", "ddd", null);
        lessonsRepository.save(lesson);
        get(createURLWithPort("/api/v1/lessons/1")).then().statusCode(200).assertThat()
                .body("description", equalTo("123"));
    }


    @Test
    @DisplayName("LessonController getLessonById time < 500 ms")
    public void whenValidateResponseTime_thenSuccess() {
        when().get(createURLWithPort("/api/v1/lessons/1")).then().time(lessThan(500L));
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
