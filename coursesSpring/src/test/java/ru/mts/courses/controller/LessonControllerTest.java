package ru.mts.courses.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.mts.courses.mapper.LessonMapper;
import ru.mts.courses.service.LessonService;
import ru.mts.courses.service.UserAuthService;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(LessonController.class)
@AutoConfigureMockMvc(addFilters = false)
public class LessonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LessonService lessonService;

    @MockBean
    private LessonMapper lessonMapper;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private UserAuthService userAuthService;

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("WebMvcTest для LessonController метод post. Позитивный тест")
    void shouldReturn200() throws Exception {

        mockMvc.perform(
                        post("/api/v1/lessons")
                                .contentType(APPLICATION_JSON)
                                .content("{\"description\":\"test\",\"content\":\"xxx\"}")
                ).andDo(print())
                .andExpect(status().is2xxSuccessful());

        verify(lessonService, times(1)).create(null);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("WebMvcTest для LessonController метод post. Должен вернуть 400 bad request")
    void shouldReturn400() throws Exception {

        mockMvc.perform(
                        post("/api/v1/lessons")
                                .contentType(APPLICATION_JSON)
                                .content("{\"description\":\"test\",\"content1\":\"xxx\"}")
                ).andDo(print())
                .andExpect(status().isBadRequest());

        verify(lessonService, times(0)).create(null);


    }



}
