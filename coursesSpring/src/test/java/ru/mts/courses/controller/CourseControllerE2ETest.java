package ru.mts.courses.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.mts.courses.domain.Course;
import ru.mts.courses.domain.CourseCategory;
import ru.mts.courses.repository.CourseCategoriesRepository;
import ru.mts.courses.repository.CoursesRepository;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;


@ExtendWith(SpringExtension.class)
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc(addFilters = false)
public class CourseControllerE2ETest {
    HttpHeaders headers = new HttpHeaders();
    TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int port;

    @Autowired
    private CoursesRepository coursesRepository;

    @Autowired
    private CourseCategoriesRepository courseCategoriesRepository;

    @Test
    @DisplayName("WebMvcTest для LessonController метод get. Позитивный тест")
    @WithMockUser(roles = "ADMIN")
    public void getLessonTest() {
        CourseCategory courseCategory = new CourseCategory(1L,"Test category", null);
        Course course = new Course(1L, "IVANOV", "Заголовок", 3L, null, true, null , null, courseCategory );
        coursesRepository.save(course);
        get(createURLWithPort("/api/v1/courses/1")).then().statusCode(200).assertThat()
                .body("title", equalTo("Заголовок"));

    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
