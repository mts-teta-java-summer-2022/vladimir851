package ru.mts.courses.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.mts.courses.domain.Course;
import ru.mts.courses.mapper.CourseMapper;
import ru.mts.courses.repository.CoursesRepository;
import ru.mts.courses.service.CourseService;
import ru.mts.courses.service.UserAuthService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CourseController.class)
@AutoConfigureMockMvc(addFilters = false)
public class CourseControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseService courseService;

    @MockBean
    private CourseMapper courseMapper;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private UserAuthService userAuthService;

    @MockBean
    Course course;

    @MockBean
    CoursesRepository coursesRepositoryMock;

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("WebMvcTest для CourseController метод post. Позитивный тест")
    void shouldReturn200() throws Exception {

        mockMvc.perform(
                        post("/api/v1/courses")
                                .contentType(APPLICATION_JSON)
                                .content("{\"title\":\"Заголовок\",\"author\":\"xxx\", \"categoryId\":1, \"duration\":3}")
                ).andDo(print())
                .andExpect(status().is2xxSuccessful());

        verify(courseService, times(1)).create(1L,null);
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    @DisplayName("WebMvcTest для CourseController метод post. Должен вернуть 400 bad request")
    void shouldReturn400() throws Exception {

        mockMvc.perform(
                        post("/api/v1/courses")
                                .contentType(APPLICATION_JSON)
                                .content("{\"title\":\"test\",\"content1\":\"xxx\"}")
                ).andDo(print())
                .andExpect(status().isBadRequest());

        verify(courseService, times(0)).create(null,null);


    }

    @Test
    void findAllCourses() {
        List<Course> courses = new ArrayList<>();
        coursesRepositoryMock = Mockito.mock(CoursesRepository.class);
        Mockito.when(coursesRepositoryMock.findAll()).thenReturn(courses);
        List<Course> resCourses = courseService.findAll();
        assertEquals(0, resCourses.size());
    }

}
