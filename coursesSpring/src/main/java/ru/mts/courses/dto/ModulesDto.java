package ru.mts.courses.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModulesDto {
    private Long id;
    private String title;
    private String description;
}
