package ru.mts.courses.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mts.courses.annotation.TitleCase;
import ru.mts.courses.domain.CourseCategory;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class CourseCreateDto {
    @NotNull(message = "Наименование курса является обязательным полем")
    @TitleCase(lang = "RU")
    private String title;

    @NotNull(message = "Автор курса является обязательным полем")
    private String author;

    @NotNull(message = "Необходимо задать продолжительность курса")
    private Long duration;

    @NotNull(message = "Необходимо задать идентификатор категории")
    private Long categoryId;

    private Long tagId;

    private Boolean visibleOnMain;
}
