package ru.mts.courses.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.mts.courses.domain.CourseCategory;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CoursesDto {
    private Long id;
    private String author;
    private String title;
    private Long duration;
    private Long categoryId;
    private Long tagId;
    private Boolean visibleOnMain;


}
