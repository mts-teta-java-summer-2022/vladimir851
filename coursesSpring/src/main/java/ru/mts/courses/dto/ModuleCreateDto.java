package ru.mts.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModuleCreateDto {
    @NotNull(message = "Наименование модуля является обязательным полем")
    private String title;

    @NotNull(message = "Описание модуля является обязательным полем")
    private String description;
}
