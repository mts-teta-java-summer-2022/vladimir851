package ru.mts.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDto {
    @NotNull(message = "Не задано имя пользователя")
    private String username;
    @NotNull(message = "Не задан пароль")
    private String password;
    private String firstname;
    private String lastname;
    private String email;

}
