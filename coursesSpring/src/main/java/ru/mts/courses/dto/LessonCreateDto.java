package ru.mts.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonCreateDto {
    @NotNull(message = "Наименование темы является обязательным полем")
    private String description;

    @NotNull(message = "Содержимое темы является обязательным полем")
    private String content;

}
