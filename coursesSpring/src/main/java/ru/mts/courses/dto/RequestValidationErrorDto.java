package ru.mts.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestValidationErrorDto {
    private List<FieldValidationErrorDto> errors;
}
