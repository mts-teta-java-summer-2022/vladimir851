package ru.mts.courses.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.domain.*;
import ru.mts.courses.domain.Module;
import ru.mts.courses.repository.UsersRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class UserService {
    private final UsersRepository usersRepository;
    private final PasswordEncoder encoder;

    @Transactional(readOnly = true)
    public List<User> findAll () {
        return usersRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<User> findById(Long id) {
        return usersRepository.findById(id);
    }


    @Transactional
    public User create(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return usersRepository.save(user);
    }

    @Transactional
    public void update(Long id, User user) {
        User userToUpdate = usersRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найден пользователь с ид " + id));
        usersRepository.save(userToUpdate);
    }

    @Transactional
    public void delete(Long id) {
        User userToDelete = usersRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найден пользователь с ид " + id));
        usersRepository.delete(userToDelete);
    }




}
