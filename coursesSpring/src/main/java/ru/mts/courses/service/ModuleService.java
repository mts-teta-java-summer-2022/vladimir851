package ru.mts.courses.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.domain.Lesson;
import ru.mts.courses.domain.Module;
import ru.mts.courses.repository.ModulesRepository;
import ru.mts.courses.repository.LessonsRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ModuleService {
    private final ModulesRepository modulesRepository;
    private final LessonsRepository lessonsRepository;

    @Transactional(readOnly = true)
    public List<Module> findAll () {
        return modulesRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Module> findById(Long id) {
        return modulesRepository.findById(id);
    }

    @Transactional
    public Module create(Module lesson) {
        return modulesRepository.save(lesson);
    }

    @Transactional
    public void update(Long id, Module module) {
        Module lessonToUpdate = modulesRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найден модуль с ид " + id));
        module.setId(id);
        modulesRepository.save(module);
    }

    @Transactional
    public void delete(Long id) {
        Module modulesToDelete = modulesRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найден модуль с ид " + id));
        modulesRepository.delete(modulesToDelete);
    }

    @Transactional
    public Module assignLesson(Long moduleId, Long lessonId) {
        Module module = modulesRepository.findById(moduleId).orElseThrow(() -> new NoSuchElementException("Не найден модуль с ид " + moduleId));
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow(() -> new NoSuchElementException("Не найдена тема с ид " + lessonId));
        module.getLesson().add(lesson);
        //  user.getCourses().add(course);
        return modulesRepository.save(module);
    }


}
