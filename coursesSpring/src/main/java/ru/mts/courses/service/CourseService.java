package ru.mts.courses.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.mts.courses.domain.Course;
import ru.mts.courses.domain.CourseCategory;
import ru.mts.courses.domain.User;
import ru.mts.courses.domain.Module;


import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.repository.CoursesRepository;
import ru.mts.courses.repository.UsersRepository;
import ru.mts.courses.repository.ModulesRepository;
import ru.mts.courses.repository.CourseCategoriesRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class CourseService {
    private final CoursesRepository coursesRepository;
    private final UsersRepository usersRepository;
    private final ModulesRepository modulesRepository;
    private final CourseCategoriesRepository courseCategoryRepository;


    @Transactional(readOnly = true)
    public List<Course> findAll () {
        return coursesRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Course> findById(Long id) {
        return coursesRepository.findById(id);
    }

    @Transactional
    public Course create(Long categoryId, Course course) {
        CourseCategory courseCategory = courseCategoryRepository.findById(categoryId).orElseThrow(() -> new NoSuchElementException("Не найдена категория с ид " + categoryId));
        course.setCourseCategory(courseCategory);
        return coursesRepository.save(course);
    }

    @Transactional
    public void update(Long id, Long categoryId, Course course) {
        CourseCategory courseCategory = courseCategoryRepository.findById(categoryId).orElseThrow(() -> new NoSuchElementException("Не найдена категория с ид " + categoryId));
        Course courseToUpdate = coursesRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найден курс с ид " + id));
        courseToUpdate.setCourseCategory(courseCategory);
        coursesRepository.save(courseToUpdate);
    }

    @Transactional
    public void delete(Long id) {
        Course courseToDelete = coursesRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найден курс с ид " + id));
        coursesRepository.delete(courseToDelete);
    }

    public List<Course> findByTitlePrefix(String titlePrefix) {
        return coursesRepository.findByTitleStartsWith(titlePrefix);
    }

    @Transactional
    public Course assignUser(Long courseId, Long userId) {
        User user = usersRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Не найден пользователь с ид " + userId));
        Course course = coursesRepository.findById(courseId).orElseThrow(() -> new NoSuchElementException("Не найден курс с ид " + courseId));
        course.getUsers().add(user);
      //  user.getCourses().add(course);
        return coursesRepository.save(course);
    }

    @Transactional
    public Course assignModule(Long courseId, Long moduleId) {
        Module module = modulesRepository.findById(moduleId).orElseThrow(() -> new NoSuchElementException("Не найден модуль с ид " + moduleId));
        Course course = coursesRepository.findById(courseId).orElseThrow(() -> new NoSuchElementException("Не найден курс с ид " + courseId));
        course.getModules().add(module);
        //  user.getCourses().add(course);
        return coursesRepository.save(course);
    }

}
