package ru.mts.courses.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mts.courses.domain.Course;
import ru.mts.courses.domain.Lesson;
import ru.mts.courses.repository.LessonsRepository;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@RequiredArgsConstructor

public class LessonService {
    private final LessonsRepository lessonsRepository;

    @Transactional(readOnly = true)
    public List<Lesson> findAll () {
        return lessonsRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Lesson> findById(Long id) {
        return lessonsRepository.findById(id);
    }

    @Transactional
    public Lesson create(Lesson lesson) {
        return lessonsRepository.save(lesson);
    }

    @Transactional
    public void update(Long id, Lesson lesson) {
        Lesson lessonToUpdate = lessonsRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найдена тема с ид " + id));
        lesson.setId(id);
        lessonsRepository.save(lesson);
    }

    @Transactional
    public void delete(Long id) {
        Lesson lessonToDelete = lessonsRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Не найдена тема с ид " + id));
        lessonsRepository.delete(lessonToDelete);
    }



}
