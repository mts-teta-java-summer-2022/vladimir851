package ru.mts.courses.domain;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "courses", schema = "courses")
public class Course {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "author")
    private String author;

    @Column(name = "title")
    private String title;

    @Column(name = "duration")
    private Long duration;

    @Column(name = "tag_id")
    private Long tagId;

    @Column(name = "visible_on_main")
    private Boolean visibleOnMain;


    @ManyToMany
    @JsonIgnore
    @JoinTable (name="users_courses",schema = "courses",
            joinColumns=@JoinColumn (name="course_id"),
            inverseJoinColumns=@JoinColumn(name="user_id"))
    private Set<User> users;

    @ManyToMany
    @JsonIgnore
    @JoinTable (name="courses_modules",schema = "courses",
            joinColumns=@JoinColumn (name="course_id"),
            inverseJoinColumns=@JoinColumn(name="module_id"))
    private Set<Module> modules;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private CourseCategory courseCategory;

}
