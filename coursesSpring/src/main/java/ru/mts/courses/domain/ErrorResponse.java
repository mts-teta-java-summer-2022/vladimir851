package ru.mts.courses.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.OffsetDateTime;

@RequiredArgsConstructor
@Getter
public class ErrorResponse {
    private final OffsetDateTime dateOccurred;
    private final String message;
}
