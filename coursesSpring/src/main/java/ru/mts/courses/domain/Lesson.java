package ru.mts.courses.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "lessons", schema = "courses")
public class Lesson {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="description")
    private String description;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name="content")
    private String content;

    @ManyToMany
    @JsonIgnore
    @JoinTable (name="modules_lessons", schema = "courses",
            joinColumns=@JoinColumn (name="lesson_id"),
            inverseJoinColumns=@JoinColumn(name="module_id"))
    private Set<Module> module;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return id.equals(lesson.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }



}
