package ru.mts.courses.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "modules", schema = "courses")
public class Module {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="title")
    private String title;

    @Column(name="description")
    private String description;

    @ManyToMany
    @JoinTable (name="courses_modules", schema = "courses",
            joinColumns=@JoinColumn (name="module_id"),
            inverseJoinColumns=@JoinColumn(name="course_id"))
    private Set<Course> course;

    @ManyToMany
    @JsonIgnore
    @JoinTable (name="modules_lessons", schema = "courses",
            joinColumns=@JoinColumn (name="module_id"),
            inverseJoinColumns=@JoinColumn(name="lesson_id"))
    private Set<Lesson> lesson;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Module module = (Module) o;
        return id.equals(module.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
