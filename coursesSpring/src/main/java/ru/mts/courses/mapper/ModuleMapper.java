package ru.mts.courses.mapper;

import org.mapstruct.Mapper;
import ru.mts.courses.domain.Module;
import ru.mts.courses.dto.ModuleCreateDto;
import ru.mts.courses.dto.ModulesDto;

@Mapper(componentModel = "spring")
public interface ModuleMapper {
    ModulesDto map(Module source);
    Module map(ModuleCreateDto source);
}
