package ru.mts.courses.mapper;

import org.mapstruct.Mapper;
import ru.mts.courses.domain.Course;
import ru.mts.courses.dto.CourseCreateDto;
import ru.mts.courses.dto.CoursesDto;

import java.util.Optional;


@Mapper(componentModel = "spring")
public interface CourseMapper {
    CoursesDto map(Course source);
    Course map(CourseCreateDto source);

    //Optional<Object> map(Object o);
}
