package ru.mts.courses.mapper;

import org.mapstruct.Mapper;
import ru.mts.courses.domain.Lesson;
import ru.mts.courses.dto.LessonsDto;
import ru.mts.courses.dto.LessonCreateDto;

@Mapper(componentModel = "spring")
public interface LessonMapper {
    LessonsDto map(Lesson source);
   Lesson map(LessonCreateDto source);
}
