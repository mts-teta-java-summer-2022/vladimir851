package ru.mts.courses.mapper;

import org.mapstruct.Mapper;
import ru.mts.courses.domain.Course;
import ru.mts.courses.domain.User;
import ru.mts.courses.dto.CourseCreateDto;
import ru.mts.courses.dto.UserCreateDto;
import ru.mts.courses.dto.UsersDto;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UsersDto map(User source);
    User map(UserCreateDto source);
}
