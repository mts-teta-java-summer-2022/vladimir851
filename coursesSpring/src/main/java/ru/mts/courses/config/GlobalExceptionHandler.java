package ru.mts.courses.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.mts.courses.domain.ErrorResponse;
import ru.mts.courses.dto.FieldValidationErrorDto;
import ru.mts.courses.dto.RequestValidationErrorDto;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.NoSuchElementException;

import static java.util.stream.Collectors.toList;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity handleMethodArgumentNotValidException(MethodArgumentNotValidException ex){
        final List<FieldValidationErrorDto> errors = ex.getFieldErrors().stream()
                .map(error -> new FieldValidationErrorDto(error.getField(), error.getDefaultMessage()))
                        .collect(toList());

                return ResponseEntity.
                        status(HttpStatus.BAD_REQUEST)
                        .body(new RequestValidationErrorDto(errors));
        }


    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleError(NoSuchElementException e) {
        return new ResponseEntity<>(new ErrorResponse(OffsetDateTime.now(), e.getMessage()) , HttpStatus.NOT_FOUND);
    }




    }

