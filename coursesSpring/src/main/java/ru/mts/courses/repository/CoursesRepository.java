package ru.mts.courses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.mts.courses.domain.Course;

import java.util.List;
import java.util.Set;

@Repository
public interface CoursesRepository extends JpaRepository<Course, Long> {
    List<Course> findByTitleStartsWith(String titlePrefix);
}
