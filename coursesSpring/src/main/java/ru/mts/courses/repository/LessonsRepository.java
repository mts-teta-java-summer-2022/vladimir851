package ru.mts.courses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mts.courses.domain.Lesson;

@Repository
public interface LessonsRepository extends JpaRepository<Lesson, Long> {
}
