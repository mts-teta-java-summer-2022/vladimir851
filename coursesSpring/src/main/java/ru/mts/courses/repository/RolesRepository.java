package ru.mts.courses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mts.courses.domain.Module;
import ru.mts.courses.domain.Role;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {

}