package ru.mts.courses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mts.courses.domain.CourseCategory;

@Repository
public interface CourseCategoriesRepository extends JpaRepository<CourseCategory, Long> {
}
