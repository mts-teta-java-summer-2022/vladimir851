package ru.mts.courses.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.mts.courses.domain.Module;

@Repository
public interface ModulesRepository extends JpaRepository<Module, Long> {

}
