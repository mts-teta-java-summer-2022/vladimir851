package ru.mts.courses.annotation;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.function.Predicate;

@Slf4j
@Setter
public class TitleCaseValidator implements ConstraintValidator<TitleCase, String> {
    private static final  String  ruLangPattern = "^[\"',:а-яА-ЯёЁ\\s]+$";
    private static final  String enLangPattern = "^[\"',:a-zA-Z\\s]+$";
    private static final  String[] excludedEnWords = new String[]{"a", "but", "for", "or", "not", "the", "an"};
    private static final  String[] excludedSymbols = new String[]{"\"", "'", ",", ":"};

    public static <T> Predicate<T> or(Predicate<T>... predicates) {
        return Arrays.stream(predicates).reduce(t -> false, Predicate::or);
    }

    // Проверка на недопустимые символы и пробелы в начале и конце строки, не более одного пробела подряд
    private final Predicate<String> validation = or(value -> value.contains("\r") ,value -> value.contains("\t"), value-> value.contains("\n") , value-> !value.equals(value.trim()) , value-> value.matches(".*\s{2,}.*") ) ;

    public enum Lang
    {
        RU, EN, ANY
    }

    Lang lang;

    @Override
    public void initialize(TitleCase constraintAnnotation) {lang = Lang.valueOf(constraintAnnotation.lang());
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        // Проверка на недопустимые символы и пробелы в начале и конце строки, не более одного пробела подряд
        if (validation.test(value))
        {

            return false;
        }

        String pureValue = pureValue(value);

        // Специфичные проверки для разных языков
        switch (lang) {
            case RU:
                // Проверка на допустимые символы
                return ruLangValidation(pureValue);
            case EN:
                return enLangValidation(pureValue);
            case ANY:
                return enLangValidation(pureValue) || ruLangValidation(pureValue);
        }
        return true;
    }

    public boolean ruLangValidation(String value) {
        int i = 0;
        if (!value.matches(ruLangPattern)) {
            return false;
        }
        // Первое слово с большой буквы, остальные с маленькой
        String[] wordsRu = value.split(" ");
        for (String word : wordsRu) {
            if ( (i==0 && word.charAt(0) == word.toLowerCase().charAt(0) )
                    || (i!=0 && word.charAt(0) == word.toUpperCase().charAt(0) )  ) {
                return false;
            }
            i++;
        }
        return true;
    }

    public boolean enLangValidation(String value) {
        // Проверка на допустимые символы
        if (!value.matches(enLangPattern)) {
            return false;
        };

        //Первое и последнее слово в заголовке должны быть написаны с большой буквы.
        String[] wordsEN = value.split(" ");
        for (String word : wordsEN) {
            if (word.charAt(0) == word.toLowerCase().charAt(0) && !(Arrays.asList(excludedEnWords).contains(word))) {
                return false;
            }
        }
        return true;
    }

    public String pureValue(String value) {
        for (String s : excludedSymbols ) {
            value = value.replace(String.valueOf(s), "");

        }
        return value;
    }


}
