package ru.mts.courses.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = TitleCaseValidator.class)
public @interface TitleCase {
    String lang();

    String message() default "Недопустимый заголовок.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
