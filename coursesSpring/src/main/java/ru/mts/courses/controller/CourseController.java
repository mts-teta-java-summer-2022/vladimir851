package ru.mts.courses.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.type.OffsetDateTimeType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.mts.courses.domain.Course;
import ru.mts.courses.domain.ErrorResponse;
import ru.mts.courses.dto.CourseCreateDto;
import ru.mts.courses.dto.CoursesDto;
import ru.mts.courses.mapper.CourseMapper;
import ru.mts.courses.service.CourseService;

import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;


import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/courses")
@Api
@Slf4j

public class CourseController {

    private final CourseService courseService;
    private final CourseMapper courseMapper;

   @GetMapping
   @ApiOperation("Получение списка всех курсов")
    public ResponseEntity<List> findAll() {

       return ResponseEntity.ok(courseService.findAll().stream()
               .map(courseMapper::map)
               .collect(toList()));
   }

    @GetMapping("/{id}")
    @ApiOperation("Получение курса по ИД")
    public ResponseEntity<CoursesDto> findById(@PathVariable Long id) {
            return ResponseEntity.of(courseService.findById(id).map(courseMapper::map));
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    @ApiOperation("Создание нового курса")
    public ResponseEntity<CoursesDto> create(@RequestBody @Valid CourseCreateDto dto) {
        final Course course = courseService.create(dto.getCategoryId(), courseMapper.map(dto));
        return ResponseEntity.ok(courseMapper.map(course));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    @ApiOperation("Удаление курса")
    public void delete(@PathVariable Long id) {
       courseService.delete(id);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    @ApiOperation("Редактирование существующего курса")
    public void update(@PathVariable Long id,
                             @Valid @RequestBody CourseCreateDto dto) {
        courseService.update(id, dto.getCategoryId(), courseMapper.map(dto));
    }

    @GetMapping("/titleByPrefix")
    public List<CoursesDto> getCoursesByTitle(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        return courseService.findByTitlePrefix(requireNonNullElse(titlePrefix, ""))
                .stream()
                .map(courseMapper::map)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{courseId}/assignUser")
    @ApiOperation("Связать курс и пользователя")
    public void assignUser(@PathVariable("courseId") Long courseId,
                             @RequestParam("userId") Long userId) {
        courseService.assignUser(courseId, userId);

    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{courseId}/assignModule")
    @ApiOperation("Связать курс и модуль")
    public void assignModule(@PathVariable("courseId") Long courseId,
                             @RequestParam("moduleId") Long moduleId) {
        courseService.assignModule(courseId, moduleId);

    }

}

