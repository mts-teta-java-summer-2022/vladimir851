package ru.mts.courses.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.mts.courses.domain.Module;
import ru.mts.courses.dto.LessonsDto;
import ru.mts.courses.dto.ModuleCreateDto;
import ru.mts.courses.dto.ModulesDto;
import ru.mts.courses.mapper.ModuleMapper;
import ru.mts.courses.service.ModuleService;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/modules")
@Api
@Slf4j
public class ModuleController {
    private final ModuleService moduleService;
    private final ModuleMapper moduleMapper;

    @GetMapping
    @ApiOperation("Получение списка всех модулей")
    public ResponseEntity<List> findAll() {

        return ResponseEntity.ok(moduleService.findAll().stream()
                .map(moduleMapper::map)
                .collect(toList()));
    }


    @GetMapping("/{id}")
    @ApiOperation("Получение модуля по ИД")
    public ResponseEntity<ModulesDto> findById(@PathVariable Long id) {
        return ResponseEntity.of(moduleService.findById(id).map(moduleMapper::map));
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    @ApiOperation("Создание нового модуля")
    public ResponseEntity<ModulesDto> create(@RequestBody @Valid ModuleCreateDto dto) {
        final Module module = moduleService.create(moduleMapper.map(dto));
        return ResponseEntity.ok(moduleMapper.map(module));
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    @ApiOperation("Редактирование существующей темы")
    public void update(@PathVariable Long id,
                       @Valid @RequestBody ModuleCreateDto dto) {
        moduleService.update(id, moduleMapper.map(dto));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    @ApiOperation("Удаление темы")
    public void delete(@PathVariable Long id) {
        moduleService.delete(id);
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{moduleId}/assignLesson")
    @ApiOperation("Связать модуль и тему")
    public void assignModule(@PathVariable("moduleId") Long courseId,
                               @RequestParam("lessonId") Long moduleId) {
         moduleService.assignLesson(courseId, moduleId);

    }


}
