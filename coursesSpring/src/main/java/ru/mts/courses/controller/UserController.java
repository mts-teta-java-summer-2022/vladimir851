package ru.mts.courses.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.mts.courses.dto.ModulesDto;
import ru.mts.courses.dto.UserCreateDto;
import ru.mts.courses.dto.UsersDto;
import ru.mts.courses.mapper.UserMapper;
import ru.mts.courses.service.UserService;
import ru.mts.courses.domain.User;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/admin/users")
@Api
@Slf4j
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping
    @ApiOperation("Получение списка всех пользователей")
    public ResponseEntity<List> findAll() {

        return ResponseEntity.ok(userService.findAll().stream()
                .map(userMapper::map)
                .collect(toList()));
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение пользователя по ИД")
    public ResponseEntity<UsersDto> findById(@PathVariable Long id) {
        return ResponseEntity.of(userService.findById(id).map(userMapper::map));
    }


    @PostMapping
    @ApiOperation("Создание нового пользователя")
    public ResponseEntity<UsersDto> create(@RequestBody @Valid UserCreateDto dto) {
        final User user = userService.create( userMapper.map(dto));
        return ResponseEntity.ok(userMapper.map(user));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление пользователя")
    public void delete(@PathVariable Long id) {
        userService.delete(id);
    }

    @PutMapping("/{id}")
    @ApiOperation("Редактирование существующего курса")
    public void update(@PathVariable Long id,
                       @Valid @RequestBody UserCreateDto dto) {
        userService.update(id, userMapper.map(dto));
    }




}
