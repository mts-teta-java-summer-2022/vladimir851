package ru.mts.courses.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import ru.mts.courses.domain.Lesson;
import ru.mts.courses.dto.CoursesDto;
import ru.mts.courses.dto.LessonCreateDto;
import ru.mts.courses.dto.LessonsDto;
import ru.mts.courses.mapper.LessonMapper;
import ru.mts.courses.service.LessonService;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/lessons")
@Api
@Slf4j

public class LessonController {
    private final LessonService lessonService;
    private final LessonMapper lessonMapper;

    @GetMapping
    @ApiOperation("Получение списка всех тем")
    public ResponseEntity<List> findAll() {

        return ResponseEntity.ok(lessonService.findAll().stream()
                .map(lessonMapper::map)
                .collect(toList()));
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение темы по ИД")
    public ResponseEntity<LessonsDto> findById(@PathVariable Long id) {
        return ResponseEntity.of(lessonService.findById(id).map(lessonMapper::map));
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    @ApiOperation("Создание новой темы")
    public ResponseEntity<LessonsDto> create(@RequestBody @Valid LessonCreateDto dto) {
        final Lesson lesson = lessonService.create(lessonMapper.map(dto));
        return ResponseEntity.ok(lessonMapper.map(lesson));
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    @ApiOperation("Редактирование существующей темы")
    public void update(@PathVariable Long id,
                       @Valid @RequestBody LessonCreateDto dto) {
                        lessonService.update(id, lessonMapper.map(dto));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    @ApiOperation("Удаление темы")
    public void delete(@PathVariable Long id) {
        lessonService.delete(id);
    }

}
