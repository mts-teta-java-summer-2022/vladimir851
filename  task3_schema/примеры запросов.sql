-- полнотекстовый поиск по наименованию категории курса

select 
	* 
from courses c 
	inner join course_categories cc on c.category_id = cc.id 
where 
	to_tsvector('russian', cc.name) @@ to_tsquery('russian', 'Базовая');
	

-- полнотекстовый поиск по наименованию курса

select 
	* 
from courses c 
	inner join course_categories cc on c.category_id = cc.id 
where 
	to_tsvector('russian', c.title) @@ to_tsquery('russian', 'верстке');
	
-- поиск по автору

select 
	* 
from courses c 
	inner join course_categories cc on c.category_id = cc.id 
where 
	c.author like 'Куз%'
	 
	
-- рейтинг курса с полнотекстовым поиском по наименованию курса
	
select c.title , avg(uc.rate) 
from courses c 
	 inner join users_courses uc on c.id = uc.course_id 
where 
	 	to_tsvector('russian', c.title) @@ to_tsquery('russian', 'верстка') and uc.rate is not null
group by c.title 

	
-- История изменений курса

select 
	*
from audit a 
	inner join object_types ot on ot.id = a.object_type_id and ot."name" = 'Курс'
	inner join courses c on c.id = a.object_id and c.title = 'курс по верстке'
	
	
-- Список пользователей, которые проходят курс
	
select
	*
from courses c 
	left join users_courses uc on uc.course_id = c.id 
	left join users u on u.id = uc.user_id
where 
	c.id = 119
	
	
-- Курс с признаком видимости на главной
	
select 
	*
from courses c 
where 
c.visible_on_main = true


-- проверка занятости email

select 
	count(*)
from 
	users u 
where 
	u.email = 'vladimir85@gmial.com'