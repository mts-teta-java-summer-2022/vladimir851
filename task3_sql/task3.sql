/* 1. Количество пользователей, которые не создали ни одного поста.*/
select 
	count(*) 
from 
	profile prof
	left join post p ON p.profile_id = prof.profile_id  
where 
	p.profile_id is null 
	
/*1. result = 5*/
	
/*2. Выберите ID всех постов по возрастанию, у которых 2 комментария, title начинается с цифры, а длина content больше 20.*/
	
select 
		p.post_id 
	from post p, comment c 
	where 
		p.post_id = c.post_id and 
		length(p."content")>20 and  
		p.title ~ '^[1-9].*' 
	group by p.post_id 
	having count(c.post_id) = 2 
	order by p.post_id asc 

/* 2. result  

|post_id|
|-------|
|22     |
|24     |
|26     |
|28     |
|32     |
|34     |
|36     |
|38     |
|42     |
|44     |
 
 */

/*3. Выберите первые 3 ID постов по возрастанию, у которых либо нет комментариев, либо он один. */
	
select 
 	p.post_id 
from post p 
	left join "comment" c on p.post_id = c.post_id 
group by 
	p.post_id 
having count(c.post_id) in (0,1)
order by p.post_id asc  
limit 3

/* 3. result

|post_id|
|-------|
|1      |
|3      |
|5      |

*/


