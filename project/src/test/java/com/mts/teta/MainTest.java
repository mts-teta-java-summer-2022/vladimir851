package com.mts.teta;

import com.mts.teta.model.Message;
import com.mts.teta.model.UserMsisdn;
import com.mts.teta.service.EnrichmentContentFactoryMSISDN;
import com.mts.teta.service.EnrichmentService;
import com.mts.teta.service.IEnrichmentContentFactory;
import org.json.JSONException;
import org.junit.jupiter.api.DisplayName;
import org.skyscreamer.jsonassert.JSONAssert;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

  @Test
  void sanityTest() {
    assertTrue(true);
  }

  // Позитивный тест. В сообщение должен добавиться блок с обогащенными данными
  @Test
  @DisplayName("Позитивный тест")
    void positiveEnrichTest () throws JSONException {
      Message message = new Message();
      message.setContent("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"79262676765\"}");
      String enrichedContent = "{\"enrichment\":{\"lastName\":\"Ivan\",\"firstName\":\"Ivanov\"}}";
      message.setEnrichmentType(Message.EnrichmentType.MSISDN);
      IEnrichmentContentFactory enrichmentContentFactory = new EnrichmentContentFactoryMSISDN(new UserMsisdn());
      EnrichmentService enrichmentService = new EnrichmentService(enrichmentContentFactory);
      String result = enrichmentService.enrich(message);
      JSONAssert.assertEquals(
              enrichedContent, result, JSONCompareMode.LENIENT);

  }

  // Негативный тест. Переданного телефона в списке нет, должно вернуться исходное сообщение
  @Test
  @DisplayName("Негативный тест")
  void negativeEnrichTest () throws JSONException {
    Message message = new Message();
    message.setContent("{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"79262676761\"}");
    String notEnrichedContent = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"79262676761\"}\"";
    message.setEnrichmentType(Message.EnrichmentType.MSISDN);
    IEnrichmentContentFactory enrichmentContentFactory = new EnrichmentContentFactoryMSISDN(new UserMsisdn());
    EnrichmentService enrichmentService = new EnrichmentService(enrichmentContentFactory);
    String result = enrichmentService.enrich(message);
    JSONAssert.assertEquals(
            notEnrichedContent, result, JSONCompareMode.LENIENT);

  }

  // Тест на многопоточность. В списках обогащенных должно быть 3 сообщения, в списке необогащенных 2
  @Test
  @DisplayName("Тест на многопоточность")
  void multiThreadTest() throws InterruptedException {
    int pools = 5;
    CountDownLatch countDownLatch = new CountDownLatch(pools);
    ExecutorService executorService = Executors.newFixedThreadPool(pools);

    IEnrichmentContentFactory enrichmentContentFactory = new EnrichmentContentFactoryMSISDN(new UserMsisdn());
    EnrichmentService enrichmentService = new EnrichmentService(enrichmentContentFactory);
    String[] contents = new String[]
            {
                    "{\"action\":\"button_click\",\"page\":\"book_card1\",\"msisdn\":\"79262676765\"}",
                    "{\"action\":\"button_click\",\"page\":\"book_card2\",\"msisdn\":\"79262676765\"}",
                    "{\"action\":\"button_click\",\"page\":\"book_card3\",\"msisdn\":\"79262676765\"}",
                    "{\"action\":\"button_click\",\"page\":\"book_card4\",\"msisdn\":\"79262676761\"}",
                    "{\"action\":\"button_click\",\"page\":\"book_card5\",\"msisdn\":\"17926267676\"}"
            };
    for (String content : contents) {
      Message message = new Message();
      message.setEnrichmentType(Message.EnrichmentType.MSISDN);
      message.setContent(content);
      executorService.submit(() -> {
        try {
          enrichmentService.enrich(message);
        } catch (Exception e) {
          fail(e);
        }
        countDownLatch.countDown();
      });
    }
    countDownLatch.await();
    executorService.shutdown();
    assertEquals(3, enrichmentService.getEnricMessages().size());
    assertEquals(2, enrichmentService.getNotEnricMessages().size());
  }



}