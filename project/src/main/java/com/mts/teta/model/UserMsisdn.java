package com.mts.teta.model;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Map;

public class UserMsisdn {
    public Map<String, User> usersMsisdn = new ConcurrentHashMap<>() {{
        put("79262676765", new User("Ivanov", "Ivan"));
        put("79032349212", new User("Petrov", "Petr"));
        put("78332223344", new User("Sidorov", "Sidr"));
    }};

    public User getUserByMsisdn(String msisdn) {
        return usersMsisdn.get(msisdn);
    }

}
