package com.mts.teta.service;
import lombok.AllArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.mts.teta.model.UserMsisdn;
import com.mts.teta.model.Message;
import com.mts.teta.model.User;

@AllArgsConstructor
public class EnrichmentContentFactoryMSISDN implements IEnrichmentContentFactory {
    public UserMsisdn userMsisdn;

    public synchronized String getEnrichedContent(Message message) {
            String msisdn = null;
            JSONObject jo = new JSONObject();
            JSONObject userJson = new JSONObject();
            try {
                Object obj = new JSONParser().parse(message.getContent());
                jo = (JSONObject) obj;
                msisdn = (String) jo.get("msisdn");
            } catch (ParseException e) {
            }

            if (msisdn != null) {
                User user = userMsisdn.getUserByMsisdn(msisdn);
                if (user != null) {
                    userJson.put("lastName", user.getLastName());
                    userJson.put("firstName", user.getFirstName());
                    jo.put("enrichment", userJson);
                }
            }
            return jo.toJSONString();
        }

}
