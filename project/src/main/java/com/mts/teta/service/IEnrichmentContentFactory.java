package com.mts.teta.service;
import com.mts.teta.model.*;

public interface IEnrichmentContentFactory {
    String getEnrichedContent(Message message) ;
}
