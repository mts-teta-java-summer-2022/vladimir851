package com.mts.teta.service;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.mts.teta.model.*;
import lombok.Getter;

@Getter
public class EnrichmentService {
    IEnrichmentContentFactory enrichmentContentFactory;

    private List<String> enricMessages;
    private List<String> notEnricMessages;

    public EnrichmentService(IEnrichmentContentFactory enrichmentContentFactory)
    {
        this.enrichmentContentFactory = enrichmentContentFactory;
        this.enricMessages = new CopyOnWriteArrayList<>();
        this.notEnricMessages = new CopyOnWriteArrayList<>();
    }

    public synchronized String enrich(Message message)   {
        String result = enrichmentContentFactory.getEnrichedContent(message);
       if (result.equals(message.getContent())) {
           notEnricMessages.add(message.getContent());
        }
       else
       {
           enricMessages.add(result);
       }
       return result;
    }
}
